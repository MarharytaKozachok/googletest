package org.googleForm.ui;

import com.googleRegister.ui.Page.GoogleFormPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilits.globalBaseTestClass.GlobalBaseTest;

import static utilits.dataGenerator.DataGenerator.*;

public class GoogleFormRegistrationTest extends GlobalBaseTest {

    GoogleFormPage googleFormPage;

    @BeforeClass
    public void preClass() {
        googleFormPage = new GoogleFormPage();
    }

    @Test(description = "Scenario: Form registration using all fields")
    public void formRegistrationUsingAllFields() {
        googleFormPage
                .openGoogleSearch()
                .enterEmail(getFakeEmail())
                .enterData(getFakerBirthdayByformat("ddmmyyyy"))
                .enterName(getFakeFirstName())
                .chooseSex("Женский")
                .chooseMood("Плохо")
                .chooseMood("Супер")
                .chooseMood("Хорошо")
                .chooseMood("Удовлетворительно")
                .chooseMood("Нормально")
                .enterMood(getFakeText())
                .clickSendButton()
                .answerSavedShouldHaveText("Ответ записан");
    }

    @Test(description = "Scenario: Form registration with empty fields")
    public void formRegistrationWithEmptyFields() {
        googleFormPage
                .openGoogleSearch()
                .clickSendButton()
                .emailErrorMessage("Это обязательный вопрос.")
                .dataPickerErrorMessage("Это обязательный вопрос.")
                .nameErrorMessage("Это обязательный вопрос.")
                .sexDropdownErrorMessage("Это обязательный вопрос.")
                .moodCheckboxErrorMessage("Это обязательный вопрос.");
    }

    @Test(description = "Scenario: Form registration with empty email field")
    public void formRegistrationWithEmptyEmailField() {
        googleFormPage
                .openGoogleSearch()
                .enterData(getFakerBirthdayByformat("ddmmyyyy"))
                .enterName(getFakeFirstName())
                .chooseSex("Женский")
                .chooseMood("Нормально")
                .clickSendButton()
                .emailErrorMessage("Это обязательный вопрос.");
    }

    @Test(description = "Scenario: Form registration with invalid email field")
    public void inputInvalidEmail() {
        googleFormPage
                .openGoogleSearch()
                .enterEmail("123")
                .emailErrorMessage("Укажите действительный адрес эл. почты");
    }

    @Test(description = "Scenario: Input future birth day date in the date field")
    public void inputFutureBirthDayDate() {
        googleFormPage
                .openGoogleSearch()
                .enterData("19/09/2994")
                .dataPickerErrorMessage("Дата Рождения не может быть в будущем");
    }

    @Test(description = "Input invalid birth day date in the date field")
    public void inputInvalidBirthDayDate() {
        googleFormPage
                .openGoogleSearch()
                .enterData("01/01/0000")
                .dataPickerErrorMessage("Введите корректную дату");
}

    @Test(description = "Scenario: Input invalid name value")
    public void inputInvalidName() {
        googleFormPage
                .openGoogleSearch()
                .enterName("@$ff%#SFDS")
                .nameErrorMessage("Введите корректное имя");
    }

    @Test(description = "Scenario: Input name consist more 20 symbols")
    public void inputNameMoreSymbols() {
        googleFormPage
                .openGoogleSearch()
                .enterName("NameValidMuxamedAliBaba")
                .nameErrorMessage("Максимальное количество символов 20 превышено");
    }

    @Test(description = "Scenario: Input name consist one symbol")
    public void inputNameConsistOneSymbol() {
        googleFormPage
                .openGoogleSearch()
                .enterName("N")
                .nameErrorMessage("Имя не может состоять из одной буквы");
    }

    @Test(description = "Scenario: Testing 'Супер' 'Нормально' check-boxes work")
    public void checkingSuperNormCheckBox() {
        googleFormPage
                .openGoogleSearch()
                .enterEmail(getFakeEmail())
                .enterData(getFakerBirthdayByformat("ddmmyyyy"))
                .enterName(getFakeFirstName())
                .chooseSex("Мужской")
                .chooseMood("Супер")
                .chooseMood("Нормально")
                .clickSendButton()
                .answerSavedShouldHaveText("Ответ записан");
    }

    @Test(description = "Scenario: Testing 'Плохо' 'Другое' check-boxes work")
    public void checkingBadAnotherCheckBox() {
        googleFormPage
                .openGoogleSearch()
                .enterEmail(getFakeEmail())
                .enterData(getFakerBirthdayByformat("ddmmyyyy"))
                .enterName(getFakeFirstName())
                .chooseSex("Женский")
                .chooseMood("Плохо")
                .enterMood(getFakeText())
                .clickSendButton()
                .answerSavedShouldHaveText("Ответ записан");
    }

    @Test(description = "Testing 'Удовлетворительно' 'Хорошо' check-boxes work")
    public void checkingAlrightGoodCheckBox() {
        googleFormPage
                .openGoogleSearch()
                .enterEmail(getFakeEmail())
                .enterData(getFakerBirthdayByformat("ddmmyyyy"))
                .enterName(getFakeFirstName())
                .chooseSex("Мужской")
                .chooseMood("Хорошо")
                .chooseMood("Удовлетворительно")
                .clickSendButton()
                .answerSavedShouldHaveText("Ответ записан");
    }

    @Test(description = "Return to registration form after click 'Отправить ещё один ответ' button")
    public void returnToRegistrationForm() {
        googleFormPage
                .openGoogleSearch()
                .enterEmail(getFakeEmail())
                .enterData(getFakerBirthdayByformat("ddmmyyyy"))
                .enterName(getFakeFirstName())
                .chooseSex("Женский")
                .chooseMood("Супер")
                .clickSendButton()
                .answerSavedShouldHaveText("Ответ записан")
                .sendAnotherAnswerLinkClick()
                .testFormNameShouldHaveText("Тест форма");
    }
}
