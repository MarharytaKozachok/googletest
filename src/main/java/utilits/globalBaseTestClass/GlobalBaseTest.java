package utilits.globalBaseTestClass;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeSuite;

public class GlobalBaseTest {

    @BeforeSuite
    public void setUp() {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.timeout = 7000;
        Configuration.headless = false;
        Configuration.startMaximized = false;
        Configuration.reportsFolder = "target/surefire-reports";
    }
}
