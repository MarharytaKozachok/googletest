package utilits.dataGenerator;

import com.github.javafaker.Faker;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataGenerator {
    private static Faker faker = new Faker();

    public static String getFakeEmail(){
        return faker.name().firstName()
                + faker.number().digits(3)
                + "@gmail.com";
    }

    public static String getFakeFirstName(){
        return faker.name().firstName();
    }

    public static String getFakerBirthdayByformat(String dateFormat){
        //  "yyyy-MM-dd"
        //  "yyyy/MM/dd"
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Date date = new Date(faker.date().birthday().getTime());
        return formatter.format(date);
    }

    public static String getFakeText(){
        return faker.chuckNorris().fact();
    }

}
