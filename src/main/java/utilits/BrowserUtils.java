package utilits;

import com.codeborne.selenide.WebDriverRunner;

public class BrowserUtils {

    public static boolean isAlertPresents() {
        try {
            WebDriverRunner.getWebDriver().switchTo().alert();
            return true;
        }// try
        catch (Exception e) {
            return false;
        }// catch
    }
}
