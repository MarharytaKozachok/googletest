package com.googleRegister.ui.Page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.matchText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static utilits.BrowserUtils.isAlertPresents;

public class GoogleFormPage {


    private By nameForm = By.cssSelector("[class *= 'HeaderDescription']");

    private By emailInput = By.name("emailAddress");
    private By datePicker = By.cssSelector("[type='date']");
    private By nameInput = By.cssSelector("[aria-label='Имя']");
    private By sexDropDown = By.cssSelector("[class *= 'exportDropDown']");
    private By anotherMoodField = By.cssSelector("[aria-label='Другой ответ']");

    private By emailErrorMessage = By.cssSelector("[class *= 'EmailCollectionField'] [role='alert']");
    private By dataPickerErrorMessage = By.id("i.err.1236342938");
    private By nameErrorMessage = By.id("i.err.1645109785");
    private By sexErrorMessage = By.id("i.err.454110266");
    private By moodErrorMessage = By.id("i.err.1001784558");


    private By answerSavedTitle = By.cssSelector("[class *= 'ConfirmationMessage']");
    private By sendAnotherAnswerTitle = By.cssSelector("[class *= 'ResponseLinksContainer'] a");


    public GoogleFormPage openGoogleSearch() {
        open("https://docs.google.com/forms/d/e/1FAIpQLSdqT5F9_qhPDmJ4lfIH7buVkUvjf4LS9ODdqD7PYfVbfFTnpA/viewform");
        if (isAlertPresents())
            Selenide.confirm();
        return new GoogleFormPage();
    }

    public GoogleFormPage enterEmail(String email) {
        $(emailInput).sendKeys(email);
        return this;
    }

    public GoogleFormPage enterData(String date) {
        $(datePicker).sendKeys(date);
        return this;
    }

    public GoogleFormPage enterName(String name) {
        $(nameInput).sendKeys(name);
        return this;
    }

    public GoogleFormPage chooseSex(String sex) {
        $(sexDropDown).click();
        sleep(500);
        $(By.cssSelector("[class*='exportSelectPopup'] [data-value='" + sex + "']")).click();
        sleep(500);
        return this;
    }

    public GoogleFormPage chooseMood(String mood) {
        $(By.cssSelector("[aria-label='" + mood + "']")).click();
        return this;
    }

    public GoogleFormPage enterMood(String anotherMood) {
        $(anotherMoodField).sendKeys(anotherMood);
        return this;
    }

    public GoogleFormPage sendAnotherAnswerLinkClick () {
        $(sendAnotherAnswerTitle).click();
        return new GoogleFormPage();
    }

    public GoogleFormPage clickSendButton() {
        $(By.cssSelector("[class*='freebird'][role='button']")).click();
        return new GoogleFormPage();
    }

    public GoogleFormPage emailErrorMessage(String errorMessage) {
        $(emailErrorMessage).shouldHave(matchText(errorMessage));
        return this;
    }

    public GoogleFormPage dataPickerErrorMessage(String errorMessage) {
        $(dataPickerErrorMessage).shouldHave(matchText(errorMessage));
        return this;
    }

    public GoogleFormPage nameErrorMessage(String errorMessage) {
        $(nameErrorMessage).shouldHave(matchText(errorMessage));
        return this;
    }

    public GoogleFormPage sexDropdownErrorMessage(String errorMessage) {
        $(sexErrorMessage).shouldHave(matchText(errorMessage));
        return this;
    }

    public GoogleFormPage moodCheckboxErrorMessage(String errorMessage) {
        $(moodErrorMessage).shouldHave(matchText(errorMessage));
        return this;
    }

    public GoogleFormPage answerSavedShouldHaveText (String text) {
        $(answerSavedTitle).shouldHave(matchText(text));
        return this;
    }

    public GoogleFormPage sendAnotherAnswerShouldHaveTextLink (String text) {
        $(answerSavedTitle).shouldHave(matchText(text));
        return this;
    }

    public GoogleFormPage testFormNameShouldHaveText (String text) {
        $(nameForm).shouldHave(Condition.visible);
        $(nameForm).shouldHave(matchText(text));
        return this;
    }




    public void sleep(int milsec) {
        System.out.println(milsec);
        Selenide.sleep(milsec);
    }
}

